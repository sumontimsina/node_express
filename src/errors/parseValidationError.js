export const parseValidationErrors = async (
    req,
    res,
    validationErrors
  ) => {
    const errors = [];
    validationErrors.details.forEach(error => {
      errors.push({
        param: error.path[0],
        message: error.message,
      });
    });
  
    return res.status(422).json({ status: 'error', statusCode: 422, errors });
  };
  