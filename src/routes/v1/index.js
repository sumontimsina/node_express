import { Router } from 'express';
// import validateToken from '../../middlewares/validateToken.js';
import userRoute from '../../module/users/user.route.js';
import authRoute from '../../module/users/auth.route.js';
import categoryRoute from '../../module/categories/category.route.js';
import validateToken from '../../middlewares/validateToken.js';

const router = Router();

router.use('/auth', authRoute);

router.use('/category', categoryRoute);

router.use('/user',validateToken, userRoute);

export default router;