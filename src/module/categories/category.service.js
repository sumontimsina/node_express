/* eslint-disable no-useless-catch */
import { Category } from './Category.model.js';
import { HttpException } from '../../errors/HttpException';
import { Sequelize } from 'sequelize';
import { sequelize } from '../../database/connection.js';
import { Product } from '../product/Product.model.js';

export const create = async (categoryData) => {
    try {
        await validateUnique(categoryData.name);
        return await Category.create(categoryData);
    } catch (err) {
        throw err;
    }
};

export const list = async (pagination, search_text) => {
    try {
        const whereStatement = {
            is_deleted: false,
        };
        if (search_text) {
            whereStatement.name = {
                [Sequelize.Op.iLike]: `%${search_text}%`,
            };
        }

        const rows = await Category.findAll({
            where: whereStatement,
            ...(pagination.limit && { limit: pagination.limit }),
            ...(pagination.offset && { offset: pagination.offset }),
        });
        const count = await Category.count({
            where: {
                is_deleted: false,
                name: {
                    [Sequelize.Op.iLike]: `%${search_text}%`,
                },
            },
        });
        console.log(rows, count);
        return { rows, count };
    } catch (err) {
        throw err;
    }
};

export const findById = async (id) => {
    try {
        return await Category.findByPk(id);
    } catch (err) {
        throw err;
    }
};

export const update = async (id, updateData) => {
    try {
        await validateUnique(updateData.name, id);
        return await Category.update(updateData, {
            where: {
                id
            }
        });
    } catch (err) {
        throw err;
    }
};

export const destroy = async (id, userId) => {
    try {
        const deleted = await Category.update({ active: false, is_deleted: true, updated_by: userId }, {
            where: {
                id
            }
        });

        await Product.update({ active: false, is_deleted: true, updated_by: userId }, {
            where: {
                category_id: id
            }
        });
        return deleted;
    } catch (err) {
        throw err;
    }
};
// Validate unique of category
const validateUnique = async (name, id = null) => {
    try {
        const [results] = await sequelize.query(`
        select id from categories 
            where lower(name) = lower('${name}') 
            and case when ${id} is not null 
                then id not in (${id}) 
            else 1=1 end
        `);

        if (results && results.length) {
            throw new HttpException(
                409,
                'Category already exist with same name'
            );
        }
        return;

    } catch (err) {
        throw err;
    }
};

