import responseHandler from '../../utils/responseHandler.js';
import * as categoryService from './category.service.js';
import { authorize } from '../../utils/authorize';
export const create = async (req, res, next) => {
    try {
        await authorize(req.user);

        const { name, brand, thumbnail } = req.body;
        const createdcategory = await categoryService.create({ name, brand, thumbnail, created_by: req.user.id });
        return res.json(responseHandler(createdcategory));

    } catch (err) {
        next(err);
    }
};

export const list = async (req, res, next) => {
    try {
        const { pagination, search_text } = req.body;
        const categories = await categoryService.list(pagination, search_text);
        return res.json(responseHandler(categories));
    } catch (err) {
        next(err);
    }
};

export const findById = async (req, res, next) => {
    try {
        const { id } = req.params;
        return await categoryService.findById(id);
    } catch (err) {
        next(err);
    }
};

export const update = async (req, res, next) => {
    try {
        await authorize(req.user);
        const { id } = req.params;
        const { name, brand, thumbnail } = req.body;

        const updated =  await categoryService.update(id, { name,brand, thumbnail, updated_by: req.user.id });
        return res.json(responseHandler(updated));

    } catch (err) {
        next(err);
    }
};

export const destroy = async (req, res, next) => {
    try {
        await authorize(req.user);
        const { id } = req.params;

        const updated =  await categoryService.destroy(id,req.user.id);
        return res.json(responseHandler(updated));

    } catch (err) {
        next(err);
    }
};
