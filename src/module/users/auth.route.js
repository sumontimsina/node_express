import { Router } from 'express';
import { userController } from '../../controller/index.js';
import { registerValidator } from './user.validator.js';

const router = Router();

router.post('/sign-up', registerValidator, userController.register);

router.post('/log-in', userController.login);

router.post('/log-in', userController.login);

export default router;