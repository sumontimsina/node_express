/* eslint-disable quotes */
import Joi from 'joi';
import { validate } from '../../utils/validate';
import { parseValidationErrors } from '../../errors/parseValidationError.js';

const educationSchema = Joi.object({
    title: Joi.string().label('Name'),
    percentage: Joi.string().label('Percentage'),
    time_ranges: Joi.string().label('time_ranges'),
    address: Joi.string().label('Adrees'),
});

const registerSchema = Joi.object({
    first_name: Joi.string().label('First Name').required(),
    last_name: Joi.string().label('Last Name').required(),
    addres: Joi.string().label('Address').required().allow(''),
    phone_number: Joi.string().label('Phone Number').required().allow(''),
    password: Joi.string().label('First Name').required(),
    email: Joi.string().label('First Name').required(),
    // For demonstration
    education: Joi.array().items(educationSchema).label('Education').min(1).messages({
        'array.base': `Education should be a type of 'array'`,
        'array.empty': `Education cannot be an empty field`,
        'array.min': `Education should have a minimum length of 1`,
        'any.required': `Education is a required field`
    })
});

export const registerValidator = (req, res, next) => {
    validate(req.body, registerSchema)
        .then(() => next())
        .catch((err) => parseValidationErrors(req, res, err));
};

