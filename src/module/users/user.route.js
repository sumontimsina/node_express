import { Router } from 'express';
import { userController } from '../../controller/index.js';

const router = Router();

router.post('/make-admin', userController.makeUserAdmin);

export default router;