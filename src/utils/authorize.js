import { HttpException } from '../errors/HttpException.js';
import { User } from '../module/users/User.model.js';

export const authorize = async (context) => {
    const user = await User.findByPk(context.id).then(res => res.dataValues);

    const isAdmin = user.is_admin;

    if (!isAdmin) {
        throw new HttpException(403, 'You are not authenticated to make this request');
    }
    return true;
};