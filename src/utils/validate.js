export const validate = async (data, schema) => {
  const { error, value } = schema.validate(data, {
    stripUnknown: true,
    abortEarly: false,
  });
  if (error) {
    throw error;
  }
  return value;
};

export default validate;
