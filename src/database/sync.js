import { User } from '../module/users/User.model.js';
import { Category } from '../module/categories/Category.model.js';
import { Product } from '../module/product/Product.model.js';

export const syncModels = async () => {
    const option = {};
    await User.sync(option);
    await Category.sync(option);
    await Product.sync(option);
};